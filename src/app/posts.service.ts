import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BlogPost } from './interfaces/blog-post';

@Injectable({
  providedIn: 'root'
})


export class PostsService {

  private API = "https://jsonplaceholder.typicode.com/posts"

  constructor(private http: HttpClient) { }

  getBlogPost(): Observable<BlogPost>
  {
  
    return this.http.get<BlogPost>(`${this.API}`);
   
  }
 
}
