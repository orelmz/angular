export interface Weather {
    name:string,
    country:string,
    image:string,
    description:string,
    temperatures:number,
    lat?:number,
    lon?:number
  
  }