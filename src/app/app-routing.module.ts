import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { CityFormComponent } from './city-form/city-form.component';
import { ClassifyComponent } from './classify/classify.component';
import { PostsComponent } from './posts/posts.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';


const routes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'classify/:channel', component: ClassifyComponent },
  { path: 'city', component: CityFormComponent },
  { path: 'signup', component: SignUpComponent},
  { path: 'posts', component: PostsComponent },
  { path: 'login', component: LoginComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
