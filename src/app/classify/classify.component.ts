import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  favoriteChannel: string;
  channels: string[] = ['BBC', 'CNN','NBC'];
  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.favoriteChannel = this.route.snapshot.params.channel
    
  }


}
