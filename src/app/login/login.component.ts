import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  email:string;
  password:string; 
  errorMessage:string;
  errorCode:string;

  onSubmit(){
    this.authService.login(this.email,this.password).then(res => {
      console.log(res)
      this.router.navigate(['/books']);

    } )
    .catch(error =>

      {
        // Handle Errors here.
        this.errorCode = error.code;
        this.errorMessage = error.message;
      
        console.log(error);
      })
  }
  
  constructor(public authService:AuthService,private router:Router) { }
  ngOnInit(): void {
  }

}