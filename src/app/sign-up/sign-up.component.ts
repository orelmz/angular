import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(public authService:AuthService,
    private router:Router) { }
hide = true;
email:string;
password:string;
errorMessage:string;
errorCode:string;

onSubmit(){
this.authService.SignUp(this.email,this.password).then(res => {
  console.log(res);
  this.router.navigate(['/books']);

} )
.catch(error =>

{
  // Handle Errors here.
  this.errorCode = error.code;
  this.errorMessage = error.message;

  console.log(error);
})


}

ngOnInit() {     
}
}