import { Weather } from './../interfaces/weather';
import { Observable } from 'rxjs';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  city:string;
  temperature:number;
  image:string;
  country:string;
  lon:number;
  lat:number;
  description:string;
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  constructor(private route:ActivatedRoute, private WeatherService:WeatherService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city
    this.weatherData$ =  this.WeatherService.searchWeatherData(this.city);
    this.weatherData$.subscribe(
      data => {
        this.temperature = data.temperatures;
        this.image = data.image;
        this.country = data.country;
        this.description = data.description;
        this.lon = data.lon;
        this.lat = data.lat;
      },
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message
      }
    )
  }

}
