import { PostsService } from './../posts.service';    

import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
 

  posts$
  


  constructor(private postsrvice:PostsService) { }

  ngOnInit() {
    this.posts$ = this.postsrvice.getBlogPost();

  }

}
