// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  _firebaseConfig: {
    apiKey: "AIzaSyBCGi6vRuopqe9OGmdPSocvZbqfrJ1uz-U",
    authDomain: "hello2-jce.firebaseapp.com",
    databaseURL: "https://hello2-jce.firebaseio.com",
    projectId: "hello2-jce",
    storageBucket: "hello2-jce.appspot.com",
    messagingSenderId: "319258834624",
    appId: "1:319258834624:web:746db44e8132a1dcb55f25"
  },
   get firebaseConfig() {
     return this._firebaseConfig;
   },
   set firebaseConfig(value) {
     this._firebaseConfig = value;
   },
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
